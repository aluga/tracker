import widetra
import numpy as np
import cv2
import glob
import time


# Tracker
address_size = 8
min_0 = 1
min_1 = 0
search_size = 12
max_discriminators = 4
max_training = 0
new_discriminator = 0.2
new_training = 0.9
min_score = 0.1
initial_scale = 0.3

print("DETECT")
cap = cv2.VideoCapture(0)
ret, frame = cap.read()
sel = cv2.selectROI(frame)
cv2.destroyAllWindows()

print("TRACKING")
# old_tracker = widetra.Tracker(frame, sel, max_discriminators, address_size, search_size, new_discriminator, new_training, max_training, min_score, min_0, min_1)
# ret, frame = cap.read()
# sel = old_tracker.find(frame)
# config1 = old_tracker.getJSON()
#
# tracker = widetra.Tracker(config1)
# config2 = tracker.getJSON()

my_tracker = widetra.Tracker(frame, sel, max_discriminators, address_size, search_size, new_discriminator, new_training, max_training, min_score, min_0, min_1, initial_scale)
ret, frame = cap.read()
sel = my_tracker.find(frame)
config1 = my_tracker.getJSON()
tracker = widetra.Tracker(config1)
config2 = tracker.getJSON()

file1 = open("config1.txt","w")
file1.write(config1)
file1.close()
file2 = open("config2.txt","w")
file2.write(config2)
file2.close()

while(True):
	ret, frame = cap.read()
	sel = tracker.find(frame)
	cv2.rectangle(frame, (sel[0], sel[1]), (sel[2], sel[3]), (0, 0, 255));
	cv2.imshow("teste", frame)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		cap.release()
		cv2.destroyAllWindows()
		break
