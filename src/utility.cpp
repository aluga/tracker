#include "util/utility.hpp"

nlohmann::json utility::getData(std::string filename) {
    std::ifstream file(filename);
    nlohmann::json jsonData;
    file >> jsonData;
    return jsonData;
}

void utility::saveData(std::string filename, nlohmann::json data) {
    std::ofstream jsonFile(filename);
    jsonFile << std::setw(4) << data << std::endl;
}

std::vector<std::string> utility::listdir(const std::string& name, int removeFirstN)
{
    std::vector<std::string> v;
    DIR* dirp = opendir(name.c_str());
    struct dirent * dp;
    while ((dp = readdir(dirp)) != NULL) {
        v.push_back(dp->d_name);
    }
    closedir(dirp);
    std::sort (v.begin(), v.end());
    return std::vector<std::string>(v.begin()+removeFirstN, v.end());
}

std::vector<cv::Rect> utility::getExpectedSelection(std::string filename, unsigned int jumpLines) {
    std::vector<int> data;
    std::vector<cv::Rect> rects;
    std::ifstream infile(filename);
    std::string line, str;
    int index = 0;
    while (getline(infile, line)) {
        if (index % jumpLines == 0) {
            std::istringstream ss (line);

            while (getline(ss, str, ','))
                data.push_back(std::atoi(str.c_str()));
            rects.push_back(cv::Rect(data[0], data[1], data[2], data[3]));
            data = std::vector<int>();
        }
        index++;
    }
    return rects;
}

std::vector<cv::Point> utility::getExpectedCenter(std::vector<cv::Rect> eS) {
    std::vector<cv::Point> centerOfRects;
    for (unsigned int i = 0; i < eS.size(); i++) centerOfRects.push_back(rectCenter(eS[i]));
    return centerOfRects;
}

std::vector<cv::Point> utility::getExpectedCenter(std::string filename, int jumpLines) {
    return getExpectedCenter(getExpectedSelection(filename, jumpLines));
}

std::string utility::getFilename(){
    auto tm = std::time(nullptr);
    auto ltm = *std::localtime(&tm);
    std::ostringstream oss;
    oss << std::put_time(&ltm, "%d-%m-%Y %H-%M-%S");
    return oss.str();
}

int utility::sum(std::vector<int> v) {
    int sumOfElems = 0;
    for (auto& n : v)
        sumOfElems += n;
    return sumOfElems;
}

long utility::ipow(int base, int exp){
    long result = 1;
    for (;;){
        if (exp & 1)
            result *= base;
        exp >>= 1;
        if (!exp)
            break;
        base *= base;
    }
    return result;
}
