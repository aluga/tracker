#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include "tracker.hpp"
#include "detector.hpp"
#include "ndarray_converter.hpp"

namespace py = pybind11;

PYBIND11_MODULE(widetra, m) {
    NDArrayConverter::init_numpy();
    m.doc() = "pybind11 example plugin"; // optional module docstring

    py::class_<Tracker>(m, "Tracker")
        .def(py::init<string>())
        .def(py::init(&Tracker::create))
        .def("find", &Tracker::findToVec)
        .def("getJSON", &Tracker::getJSON);

    py::class_<Detector>(m, "Detector")
        .def(py::init<string>())
        .def(py::init(&Detector::create))
        .def("find", &Detector::findToVec)
        .def("train", &Detector::train)
        .def("getJSON", &Detector::getJSON);
}
