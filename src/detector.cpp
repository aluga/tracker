#include "detector.hpp"


Detector::Detector(std::string config){
    json c = json::parse(config);

    this->addressSize = c["addressSize"];
    this->entrySize = c["entrySize"];
    this->min_0 = c["min_0"];
    this->min_1 = c["min_1"];

    this->slidingWindows.push_back(cv::Rect(0, 0, c["slidingWindows"]["width"], c["slidingWindows"]["height"]));

    json dsc = c["discriminators"];
    json rbase = {
      {"min_0", c["min_0"]},
      {"min_1", c["min_1"]}
    };
    // this->discriminators.resize(c.size());
    for(json::iterator it = dsc.begin(); it != dsc.end(); ++it){
        json d = it.value();
        d.merge_patch(rbase);
        this->discriminators.push_back(Discriminator(d));
    }
    binaryFunc = &utility::binarization;
}

Detector::Detector(int addressSize, int min_0, int min_1, int binType): addressSize(addressSize), min_0(min_0), min_1(min_1){
    this->slidingWindows.push_back(cv::Rect(0, 0, 0, 0));
    switch (binType) {
        case 0:
            binaryFunc = &utility::binarization;
            break;
        case 1:
            binaryFunc = &utility::hogFeatures;
            break;
    }
}

cv::Rect Detector::find(cv::Mat frame, int step){
    int originalStep = step;
    cv::Rect bestSelection;
    int bestScore = 0,
        tempScore = 0;

    int xMax, yMax;
    std::vector<int> object;

    xMax = frame.size().width - this->slidingWindows[0].width;
    yMax = frame.size().height - this->slidingWindows[0].height;
    // std::cout << xMax << " " << yMax << '\n';
    // std::cout << this->slidingWindows[0].width << " " << this->slidingWindows[0].height << '\n';
    for (this->slidingWindows[0].y = 0; this->slidingWindows[0].y < yMax; this->slidingWindows[0].y += step) {
        for (this->slidingWindows[0].x = 0; this->slidingWindows[0].x < xMax; this->slidingWindows[0].x += step) {
            object = (*binaryFunc)(frame(this->slidingWindows[0]));
            tempScore = this->discriminators[0].getVotes(object);
            if (tempScore > bestScore) {
                bestScore = tempScore;
                bestSelection = cv::Rect(this->slidingWindows[0].x, this->slidingWindows[0].y, this->slidingWindows[0].width, this->slidingWindows[0].height);
                if (step > 1) step /= 2;
                if (this->slidingWindows[0].x > step) this->slidingWindows[0].x -= step;
                if (this->slidingWindows[0].y > step) this->slidingWindows[0].y -= step;
            } else {
                step = originalStep;
            }
        }
    }
    return bestSelection;
}

std::vector<int> Detector::findToVec(cv::Mat frame, int step, double frameScale){
    cv::resize(frame, frame, cv::Size(frame.size().width*frameScale, frame.size().height*frameScale));
    cv::Rect r = this->find(frame, step);
    return {(int)(r.x/frameScale), (int)(r.y/frameScale), (int)((r.x + r.width)/frameScale), (int)((r.y + r.height)/frameScale)};
}

void Detector::train(cv::Mat img){
    std::vector<int> obj = (*binaryFunc)(img);
    this->slidingWindows[0].width = img.size().width;
    this->slidingWindows[0].height = img.size().height;
    if (this->discriminators.size() == 0) {
        this->discriminators.push_back(Discriminator(addressSize, obj.size(), this->min_0, this->min_1));
    }
    this->discriminators[0].train(obj);
}

json Detector::getConfig(){
    json config = {
        {"addressSize", addressSize},
        {"entrySize", entrySize},
        {"min_0", min_0},
        {"min_1", min_1}
    };
    return config;
}

json Detector::getDiscriminatorsJSON(){
    json d;
    for(size_t i = 0; i < this->discriminators.size(); i++){
        d[i] = this->discriminators[i].getJSON();
    }
    return d;
}

json Detector::getSlidingWindowsJSON(){
    json s;
    s["width"] = this->slidingWindows[0].width;
    s["height"] = this->slidingWindows[0].height;
    return s;
}

std::string Detector::getJSON(){
    json config = getConfig();
    config["slidingWindows"] = getSlidingWindowsJSON();
    config["discriminators"] = getDiscriminatorsJSON();
    return config.dump(2);
}
