#include "tracker.hpp"

Tracker::Tracker(){}

Tracker::Tracker(std::string config) {
    json c = json::parse(config);

    this->addressSize = c["addressSize"];
    this->searchSize = c["searchSize"];
    this->entrySize = c["entrySize"];
    this->maxDiscriminators = c["maxDiscriminators"];
    this->bestDiscriminator = c["bestDiscriminator"];
    this->bestScore = c["bestScore"];
    this->newTraining = c["newTraining"];
    this->maxTraining = c["maxTraining"];
    this->newDiscriminator = c["newDiscriminator"];
    this->minScore = c["minScore"];
    this->frameWidth = c["frameWidth"];
    this->frameHeight = c["frameHeight"];
    this->initialScale = c["initialScale"];
    this->min_0 = c["min_0"];
    this->min_1 = c["min_1"];

    this->selection = cv::Rect(c["selection"]["x"], c["selection"]["y"], c["selection"]["w"], c["selection"]["h"]);

    json dsc = c["discriminators"];
    json rbase = {
      {"min_0", c["min_0"]},
      {"min_1", c["min_1"]}
    };
    // this->discriminators.resize(c.size());
    for(json::iterator it = dsc.begin(); it != dsc.end(); ++it){
        json d = it.value();
        d.merge_patch(rbase);
        this->discriminators.push_back(Discriminator(d));
    }
}

Tracker::Tracker(cv::Mat initialFrame, cv::Rect selection, int maxDiscriminators,
        int addressSize, int searchSize, double newDiscriminator,
        double newTraining, int maxTraining, double minScore, int min_0, int min_1, double initialScale)
    : searchSize(searchSize), min_0(min_0), min_1(min_1), addressSize(addressSize), maxDiscriminators(maxDiscriminators), maxTraining(maxTraining), initialScale(initialScale), bestDiscriminator(0){
    this->resetParams();

    this->frameWidth = initialFrame.size().width * this->initialScale;
    this->frameHeight = initialFrame.size().height * this->initialScale;

    cv::resize(initialFrame, initialFrame, cv::Size(this->frameWidth, this->frameHeight));

    this->selection = cv::Rect(selection.x*this->initialScale, selection.y*this->initialScale, selection.width*this->initialScale, selection.height*this->initialScale);

    cv::Rect limits = this->calculateLimits();
    cv::Mat searchArea(utility::preprocessing(initialFrame(limits)));

    std::vector<int> obj = searchArea(cv::Rect(this->selection.x - limits.x, this->selection.y - limits.y, this->selection.width, this->selection.height)).clone().reshape(1,1);
    this->entrySize = obj.size();

    int numberOfRams = this->entrySize / addressSize;
    if (this->entrySize % addressSize > 0) numberOfRams++;

    this->newTraining = newTraining * numberOfRams;
    this->newDiscriminator = newDiscriminator * numberOfRams;
    this->minScore = minScore * numberOfRams;

    this->updateDiscriminators(obj);
}

cv::Rect Tracker::find(cv::Mat frame){
    cv::resize(frame, frame, cv::Size(this->frameWidth, this->frameHeight));

    int localScore = 0;

    cv::Rect limits = this->calculateLimits(), localSelection(0, 0, this->selection.width, this->selection.height), bestSelection;
    cv::Mat searchArea(utility::preprocessing(frame(limits)));

    const unsigned int  yLimit = searchArea.size().height - this->selection.height,
                        xLimit = searchArea.size().width - this->selection.width;

    for (unsigned int y = 0; y <= yLimit; y++) {
        localSelection.y = y;
        for (unsigned int x = 0; x <= xLimit; x++) {
            localSelection.x = x;
            this->recognize(searchArea(localSelection).clone().reshape(1,1));
            if (this->bestScore > localScore) {
                localScore = this->bestScore;
                bestSelection = localSelection;
            }
        }
    }

    // std::cout << this->bestScore << '\n';
    // if (this->bestScore < this->minScore) return cv::Rect(0, 0, 0, 0);

    this->updateDiscriminators(searchArea(bestSelection).clone().reshape(1,1));

    this->selection = bestSelection;
    this->selection.x += limits.x;
    this->selection.y += limits.y;

    return cv::Rect(this->selection.x/this->initialScale,
                    this->selection.y/this->initialScale,
                    this->selection.width/this->initialScale,
                    this->selection.height/this->initialScale);
}

std::vector<int> Tracker::findToVec(cv::Mat frame){
    cv::Rect r = this->find(frame);
    return std::vector<int>({r.x, r.y, r.x + r.width, r.y + r.height});
}

void Tracker::recognize(std::vector<int> object) {
    int localScore = 0;
    for (size_t i = 0; i < this->discriminators.size(); i++) {
        localScore = this->discriminators[i].getVotes(object);
        if (localScore > this->bestScore) {
            this->bestScore = localScore;
            this->bestDiscriminator = i;
        }
    }
}

void Tracker::updateDiscriminators(std::vector<int> object) {
    if (this->bestScore < this->newDiscriminator) {
        if (this->discriminators.size() >= this->maxDiscriminators) this->discriminators.pop_back();
        this->discriminators.push_front(Discriminator(this->addressSize, this->entrySize, this->min_0, this->min_1));
    } else if (this->bestDiscriminator > 0) {
        this->discriminators.push_front(this->discriminators[this->bestDiscriminator]);
        this->discriminators.erase(this->discriminators.begin() + this->bestDiscriminator + 1);
    }

    if (this->bestScore < this->newTraining && (this->maxTraining == 0 || this->discriminators[0].getNumberOfTrainings() < this->maxTraining)) {
        this->discriminators[0].train(object);
    }

    this->resetParams();
}

void Tracker::resetParams(){
    this->bestDiscriminator = -1;
    this->bestScore = 0;
}

json Tracker::getConfig(){
    json config = {
        {"addressSize", addressSize},
        {"searchSize", searchSize},
        {"entrySize", entrySize},
        {"maxDiscriminators", maxDiscriminators},
        {"bestDiscriminator", bestDiscriminator},
        {"bestScore", bestScore},
        {"newTraining", newTraining},
        {"maxTraining", maxTraining},
        {"newDiscriminator", newDiscriminator},
        {"minScore", minScore},
        {"min_0", min_0},
        {"min_1", min_1},
        {"frameWidth", frameWidth},
        {"frameHeight", frameHeight},
        {"initialScale", initialScale}
    };
    return config;
}

json Tracker::getDiscriminatorsJSON(){
    json d;
    for(size_t i = 0; i < this->discriminators.size(); i++){
        d[i] = this->discriminators[i].getJSON();
    }
    return d;
}

json Tracker::getSelectionJSON(){
    json s;
    s["x"] = this->selection.x;
    s["y"] = this->selection.y;
    s["w"] = this->selection.width;
    s["h"] = this->selection.height;
    return s;
}

std::string Tracker::getJSON(){
    json config = getConfig();
    config["selection"] = getSelectionJSON();
    config["discriminators"] = getDiscriminatorsJSON();
    return config.dump(2);
}
