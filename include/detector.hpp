#ifndef _DETECTOR_H_
#define _DETECTOR_H_

#include <opencv2/opencv.hpp>
#include "wisard/discriminator.hpp"
#include "util/utility.hpp"

class Detector{
public:
    Detector(std::string config);
    
    Detector(int addressSize, int min_0, int min_1, int binType);

    static Detector create(int _addressSize, int _min_0, int _min_1, int _binType){
        return Detector(_addressSize, _min_0, _min_1, _binType);
    }

    cv::Rect find(cv::Mat frame, int step);

    std::vector<int> findToVec(cv::Mat frame, int step, double frameScale=1.0);

    void train(cv::Mat img);

    json getConfig();

    json getDiscriminatorsJSON();

    json getSlidingWindowsJSON();

    std::string getJSON();

private:
    std::vector<cv::Rect> slidingWindows;
    int addressSize, entrySize;
    std::vector<Discriminator> discriminators;
    int min_0, min_1;
    std::vector<int> (*binaryFunc)(cv::Mat);
};

#endif
