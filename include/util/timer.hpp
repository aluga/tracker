#ifndef _TIMER_H_
#define _TIMER_H_

#include <chrono>

class Timer {
public:
    typedef enum measure_type
    {
        MICROSECONDS=0,
        MILLISECONDS=1,
        SECONDS=2
    } Measure;

    Timer() : start(clock::now()) {}

    void reset() {
        this->start = clock::now();
    }

    uint64_t getTime(measure_type m = MICROSECONDS) const {
        if(m == SECONDS)
            return std::chrono::duration_cast<seconds>(clock::now() - this->start).count();
        else if(m == MILLISECONDS)
            return std::chrono::duration_cast<milliseconds>(clock::now() - this->start).count();
        return std::chrono::duration_cast<microseconds>(clock::now() - this->start).count();
    }

private:
    typedef std::chrono::steady_clock clock;
    typedef std::chrono::microseconds microseconds;
    typedef std::chrono::milliseconds milliseconds;
    typedef std::chrono::seconds seconds;

    clock::time_point start;
};

#endif
