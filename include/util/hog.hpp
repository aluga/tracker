#ifndef HOG_H
#define HOG_H

#define PI 3.14159

#include <opencv2/opencv.hpp>

class HOG {
public:
	HOG(double threshold=0.1);
	void run(cv::Mat & inputImage);
	std::vector<cv::Mat> getFeatureVec();
	std::vector<int> getBinaryFeatureVec();
private:
	int nwin_x, nwin_y, B;
	double threshold;
	std::vector<cv::Mat> hogFeatureVec;
	cv::Mat getFeature(cv::Mat & inputImage);
};

#endif
