#ifndef _UTILITY_H_
#define _UTILITY_H_

#include "json.hpp"
#include "hog.hpp"
#include <fstream>

#include <sys/types.h>
#include <dirent.h>
#include <opencv2/opencv.hpp>
#include <string>

namespace utility {
    nlohmann::json getData(std::string filename);

    void saveData(std::string filename, nlohmann::json data);

    std::vector<std::string> listdir(const std::string& name, int removeFirstN = 2);

    inline std::vector<int> hogFeatures(cv::Mat image) {
        HOG myHog;
        cv::cvtColor(image, image, cv::COLOR_RGB2GRAY);
        myHog.run(image);
        return myHog.getBinaryFeatureVec();
    }

    inline std::vector<int> binarization(cv::Mat image) {
        cv::Mat binary;
        cv::cvtColor(image, image, cv::COLOR_RGB2GRAY);
        cv::adaptiveThreshold(image, binary, 1, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY, 11, 5);
        return binary.clone().reshape(1,1);
    }

    inline cv::Mat preprocessing(cv::Mat image) {
        cv::Mat binary;
        cv::cvtColor(image, image, cv::COLOR_RGB2GRAY);
        cv::adaptiveThreshold(image, binary, 1, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY, 11, 5);
        return binary;
    }

    inline int euclideanDistance(cv::Point a, cv::Point b) {
    	return sqrt(pow(a.x - b.x, 2) + pow(a.y - b.y, 2));
    }

    inline cv::Point rectCenter(cv::Rect r) {
        return cv::Point(r.br() + r.tl())*0.5;
    }

    std::vector<cv::Rect> getExpectedSelection(std::string filename, unsigned int jumpLines=5);

    std::vector<cv::Point> getExpectedCenter(std::vector<cv::Rect> eS);

    std::vector<cv::Point> getExpectedCenter(std::string filename, int jumpLines=5);

    std::string getFilename();

    int sum(std::vector<int> v);

    inline int randint(int min, int max){
        std::srand(time(NULL));
        return min + (std::rand() % (int)(max - min + 1));
    }

    long ipow(int base, int exp);

}

#endif
