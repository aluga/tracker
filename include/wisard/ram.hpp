#ifndef _WISARD_H_
#define _WISARD_H_

#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <tuple>
#include <cstdlib>
#include "util/json.hpp"
#include "util/utility.hpp"

using namespace std;
using json = nlohmann::json;

class RAM{
public:
  RAM(){}
  RAM(json c){
    this->min_0 = c["min_0"];
    this->min_1 = c["min_1"];
    this->addresses = c["addresses"].get<vector<int>>();
    checkLimitAddressSize(addresses.size(), 2);
    json pos = c["positions"];
    for(json::iterator it = pos.begin(); it != pos.end(); ++it){
      unsigned int p = stoi(it.key());
      positions[p] = it.value();
    }
  }

  RAM(const int addressSize, const int entrySize, const int min_0, const int min_1): min_0(entrySize - min_0), min_1(min_1){
    checkLimitAddressSize(addressSize, 2);
    this->addresses = vector<int>(addressSize);
    generateRandomAddresses(entrySize);
  }

  RAM(const vector<int>& indexes, const int min_0, const int min_1): addresses(indexes), min_0(indexes.size() - min_0), min_1(min_1){
    checkLimitAddressSize(indexes.size(), 2);
  }

  int getVote(const vector<int>& image){
    return this->getScore(image) > 0 ? 1 : 0;
  }

  int getScore(const vector<int>& image){
    std::vector<unsigned int> ic = this->getIndex(image);
    if(ic[1] > this->min_0 || ic[1] < this->min_1) return 0;
    auto it = positions.find(ic[0]);
    if(it == positions.end()) return 0;
    else return it->second;
  }

  void train(const vector<int>& image){
    std::vector<unsigned int> ic = this->getIndex(image);
    auto it = this->positions.find(ic[0]);
    if(it == this->positions.end()){
      this->positions.insert(it,pair<unsigned int,int>(ic[0], 1));
    }
    else{
      it->second++;
    }
  }

  void untrain(const vector<int>& image){
      std::vector<unsigned int> ic = this->getIndex(image);
      auto it = this->positions.find(ic[0]);
      if(it != this->positions.end()){
        it->second--;
      }
  }

  vector<vector<int>>& getMentalImage() {
    vector<vector<int>>* mentalPiece = new vector<vector<int>>(this->addresses.size());
    for(unsigned int i = 0; i < mentalPiece->size(); i++){
      (*mentalPiece)[i].resize(2);
      (*mentalPiece)[i][0] = this->addresses[i];
      (*mentalPiece)[i][1] = 0;
    }

    for(auto j = this->positions.begin(); j != this->positions.end(); ++j){
      if(j->first == 0) continue;
      const vector<int> address = convertToBase(j->first);
      for(unsigned int i = 0; i < mentalPiece->size(); i++){
        if((*mentalPiece)[i].size() == 0){
          (*mentalPiece)[i].resize(2);
          (*mentalPiece)[i][0] = this->addresses[i];
          (*mentalPiece)[i][1] = 0;
        }
        if(address[i] > 0){
          (*mentalPiece)[i][1] += j->second;
        }
      }
    }
    return *mentalPiece;
  }

  json positionsToJSON(){
    json pos;
    for(auto j = this->positions.begin(); j != this->positions.end(); ++j){
      pos[to_string(j->first)] = j->second;
    }
    return pos;
  }

  // json getConfig(){
  //   json config = {
  //     {"min_0", this->min_0},
  //     {"min_1", this->min_1}
  //   };
  //   return config;
  // }

  json getJSON(){
    json config = {
      {"addresses", this->addresses},
      {"positions", nullptr}
    };
    config["positions"] = positionsToJSON();
    return config;
  }

protected:
  std::vector<unsigned int> getIndex(const vector<int>& image) const{
    unsigned int index = 0, p = 1, count = 0;
    for(unsigned int i = 0; i < this->addresses.size(); i++){
      int bin = image[this->addresses[i]];
      checkPos(bin);
      index += bin*p;
      p *= 2;
      count += bin;
    }
    return {index, count};
  }


private:
  vector<int> addresses;
  unordered_map<unsigned int,int> positions;
  int min_0, min_1;

  const vector<int> convertToBase(const int number) const{
    vector<int> numberConverted(this->addresses.size());
    int baseNumber = number;
    for(unsigned int i = 0; i < numberConverted.size(); i++){
      numberConverted[i] = baseNumber % 2;
      baseNumber /= 2;
    }
    return numberConverted;
  }

  void checkLimitAddressSize(int addressSize, int basein){
    long limit = 2l << 31;
    if(addressSize > 31 || 2 > 1626 || utility::ipow(basein, addressSize) > limit){
      throw std::invalid_argument("The 2 power to addressSize passed the limit of 2^31!");
    }
  }

  void checkPos(const int code) const{
    if(code >= 2){
      throw std::invalid_argument("The input data has a value bigger than 2 of addresing!");
    }
  }

  void generateRandomAddresses(int entrySize){
    for(unsigned int i = 0; i < this->addresses.size(); i++){
      this->addresses[i] = utility::randint(0, entrySize-1);
    }
  }
};

#endif
