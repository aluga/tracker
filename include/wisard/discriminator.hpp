#ifndef _DISCRIMINATOR_H_
#define _DISCRIMINATOR_H_

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <tuple>
#include <cstdlib>
#include "ram.hpp"

using namespace std;

using json = nlohmann::json;

class Discriminator{
public:
  Discriminator(): entrySize(0){}

  Discriminator(string config):Discriminator(json::parse(config)){}

  Discriminator(json config){
    entrySize = config["entrySize"];
    numberOfTrainings = config["numberOfTrainings"];
    json jrams = config["rams"];
    json rbase = {
      {"min_0", config["min_0"]},
      {"min_1", config["min_1"]}
    };
    for(json::iterator it = jrams.begin(); it != jrams.end(); ++it){
      json base = *it;
      base.merge_patch(rbase);
      rams.push_back(RAM(base));
    }
  }

  Discriminator(int addressSize, int entrySize, int min_0, int min_1): entrySize(entrySize), min_0(min_0), min_1(min_1){
    setRAMShuffle(addressSize);
  }

  Discriminator(vector<int> indexes, int addressSize, int entrySize, int min_0, int min_1): entrySize(entrySize), min_0(min_0), min_1(min_1){
    setRAMByIndex(indexes, addressSize);
  }

  void setRAMShuffle(int addressSize){
    checkAddressSize(addressSize);

    int numberOfRAMS = this->entrySize / addressSize;
    int remain = this->entrySize % addressSize;
    int indexesSize = this->entrySize;

    if(remain > 0) {
      numberOfRAMS++;
      indexesSize += addressSize - remain;
    }

    rams.resize(numberOfRAMS);
    vector<int> indexes(indexesSize);

    for(int i = 0; i < this->entrySize; i++) {
      indexes[i] = i;
    }

    for(unsigned int i = this->entrySize; i < indexes.size(); i++){
      indexes[i] = utility::randint(0, this->entrySize-1);
    }

    random_shuffle(indexes.begin(), indexes.end());

    for(unsigned int i=0; i<rams.size(); i++){
      vector<int>* subIndexes = new vector<int>(indexes.begin() + (i*addressSize), indexes.begin() + ((i+1)*addressSize));
      rams[i] = RAM(*subIndexes, this->min_0, this->min_1);
    }
  }

  void setRAMByIndex(vector<int> indexes, int addressSize){
    checkAddressSize(addressSize);
    checkListOfIndexes(indexes);

    int numberOfRAMS = this->entrySize / addressSize;
    rams = vector<RAM>(numberOfRAMS);

    for(unsigned int i=0; i<rams.size(); i++){
      vector<int>* subIndexes = new vector<int>(indexes.begin() + (i*addressSize), indexes.begin() + ((i+1)*addressSize));
      rams[i] = RAM(*subIndexes);
    }
  }

  int getVotes(const vector<int>& image) {
    checkEntrySize(image.size());
    int votes = 0;
    for(unsigned int i=0; i<rams.size(); i++){
      votes += rams[i].getVote(image);
    }
    return votes;
  }

  void train(const vector<int>& image){
    checkEntrySize(image.size());
    numberOfTrainings++;
    for(unsigned int i=0; i<rams.size(); i++){
      rams[i].train(image);
    }
  }

  void train(const vector<vector<int>>& image){
    for(unsigned int i=0; i<image.size(); i++){
      train(image[i]);
    }
  }

  void untrain(const vector<int>& image){
      checkEntrySize(image.size());
      numberOfTrainings--;
      for(unsigned int i=0; i<rams.size(); i++){
        rams[i].untrain(image);
      }
  }

  int getNumberOfTrainings() const{
    return numberOfTrainings;
  }

  int getNumberOfRAMS() const{
    return rams.size();
  }

  vector<int>& getMentalImage(){
    vector<int>* mentalImage = new vector<int>(entrySize);
    for(unsigned int i=0; i<mentalImage->size(); i++) {
      (*mentalImage)[i] = 0;
    }

    for(unsigned int r=0; r<rams.size(); r++){
      vector<vector<int>> piece = rams[r].getMentalImage();
      for(vector<int> p: piece){
        (*mentalImage)[p[0]] += p[1];
      }
    }
    return *mentalImage;
  }

  json getRAMSJSON(){
    json rj = json::array();
    for(unsigned int i=0; i<rams.size(); i++){
      rj[i] = rams[i].getJSON();
    }
    return rj;
  }

  json getConfig(){
    json config = {
      {"entrySize", entrySize},
      {"numberOfTrainings", numberOfTrainings}
    };
    return config;
  }

  // string getConfigString(){
  //   json config = getConfig();
  //   if(!rams.empty()){
  //     config.merge_patch(rams[0].getConfig());
  //   }
  //   config["rams"] = getRAMSJSON(false);
  //   return config.dump(2);
  // }
  //
  // string getJSONString(){
  //   json config = getConfig();
  //   if(!rams.empty()){
  //     config.merge_patch(rams[0].getConfig());
  //   }
  //   config["rams"] = getRAMSJSON();
  //   return config.dump(2);
  // }

  // json getConfigJSON(){
  //   json config = getConfig();
  //   config["rams"] = getRAMSJSON(false);
  //   return config;
  // }

  json getJSON(){
    json config = getConfig();
    config["rams"] = getRAMSJSON();
    return config;
  }
private:

  void checkEntrySize(const int entry) const {
    if(this->entrySize != entry){
      throw std::invalid_argument("The entry size defined on creation of discriminator is different of entry size given as input!");
    }
  }

  void checkAddressSize(const int addressSize) const{
    if( addressSize < 2){
      throw std::invalid_argument("The address size cann't be lesser than 2!");
    }
    if( this->entrySize < 2 ){
      throw std::invalid_argument("The entry size cann't be lesser than 2!");
    }
    if( this->entrySize < addressSize){
      throw std::invalid_argument("The address size cann't be bigger than entry size!");
    }
  }

  void checkListOfIndexes(const vector<int>& indexes) const{
    if((int)indexes.size() != this->entrySize){
      throw std::invalid_argument("The list of indexes is not compatible with entry size!");
    }

    map<int, int> values;
    for(unsigned int i=0; i<indexes.size(); i++){
      if(indexes[i] >= this->entrySize){
        throw std::invalid_argument("The list of indexes has a index out of range of entry!");
      }
      if(values.find(indexes[i]) == values.end()){
        values[indexes[i]] = i;
      }
      else{
        throw std::invalid_argument("The list of indexes contain repeated indexes!");
      }
    }
  }

  int entrySize, numberOfTrainings;
  int min_0, min_1;
  vector<RAM> rams;
};

#endif
