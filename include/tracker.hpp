#ifndef _TRACKER_H_
#define _TRACKER_H_

#include <opencv2/opencv.hpp>
#include <deque>
#include "util/utility.hpp"
#include "util/json.hpp"
#include "wisard/discriminator.hpp"

// TODO: multitracking
class Tracker{
public:
    Tracker();

    Tracker(cv::Mat initialFrame, cv::Rect selection, int maxDiscriminators,
            int addressSize, int searchSize, double newDiscriminator,
            double newTraining, int maxTraining, double minScore, int min_0, int min_1, double initialScale);

    Tracker(std::string config);

    static Tracker create(cv::Mat _initialFrame, std::vector<int> _selection, int _maxDiscriminators,
                            int _addressSize, int _searchSize, double _newDiscriminator,
                            double _newTraining, int _maxTraining, double _minScore, int _min_0, int _min_1, double _initialScale){
        return Tracker(_initialFrame, cv::Rect(_selection[0], _selection[1], _selection[2], _selection[3]),
                        _maxDiscriminators, _addressSize, _searchSize, _newDiscriminator, _newTraining, _maxTraining, _minScore, _min_0, _min_1, _initialScale);
    }

    cv::Rect find(cv::Mat frame);

    std::vector<int> findToVec(cv::Mat frame);

    int getNumberOfDiscriminators() const{
        return this->discriminators.size();
    }

    json getConfig();

    json getSelectionJSON();

    json getDiscriminatorsJSON();

    std::string getJSON();

private:
    cv::Rect selection;
    int searchSize, min_0, min_1, addressSize, entrySize, maxDiscriminators;
    int bestDiscriminator, bestScore;
    int newTraining, maxTraining, newDiscriminator, minScore;
    std::deque<Discriminator> discriminators;
    int frameWidth, frameHeight;
    double initialScale;

    void updateDiscriminators(std::vector<int> object);

    void resetParams();

    void recognize(std::vector<int> object);

    inline cv::Rect calculateLimits(){
        int xMin = this->selection.x - this->searchSize,
            yMin = this->selection.y - this->searchSize;

        if (xMin < 0) xMin = 0;
        if (yMin < 0) yMin = 0;

        int xMax = (this->selection.x + this->selection.width + this->searchSize) - xMin,
            yMax = (this->selection.y + this->selection.height + this->searchSize) - yMin;

        if (xMin + xMax > this->frameWidth) xMax = this->frameWidth - xMin;
        if (yMin + yMax > this->frameHeight) yMax = this->frameHeight - yMin;

        return cv::Rect(xMin, yMin, xMax, yMax);
    }
};


#endif
