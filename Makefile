SRC_PATH = src
SRC_EXT = cpp
SOURCES = $(shell find $(SRC_PATH) -name '*.$(SRC_EXT)' | sort -k 1nr | cut -f2-)
INCLUDES = -I include/ -I /usr/local/include
LIBS = `pkg-config opencv --cflags --libs`

PY_CC ?= c++
PY_FLAGS = -m64 -g -Wall -O3 -shared -std=c++11 -fPIC `python3 -m pybind11 --includes` -w
EXEC_PY = `python3-config --extension-suffix`
PY_BIN_NAME = widetra$(EXEC_PY)
PY_SRC_EXT = cc
PY_SRC_PATH = src
PY_SOURCES = $(shell find $(PY_SRC_PATH) -name '*.$(PY_SRC_EXT)' | sort -k 1nr | cut -f2-)
PY_INCLUDES = -I/usr/include/python3.5

'-':
	$(PY_CC) $(PY_FLAGS) -o $(PY_BIN_NAME) $(PY_SOURCES) $(SOURCES) $(INCLUDES) $(PY_INCLUDES) $(LIBS)

clean:
	@rm $(PY_BIN_NAME)
