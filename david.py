import widetra
import numpy as np
import cv2
import glob
import time
import math

# Tracker
address_size = 8
min_0 = 1
min_1 = 0
search_size = 12
max_discriminators = 4
max_training = 0
new_discriminator = 0.2
new_training = 0.9
min_score = 0.1
initial_scale = 0.3

# Detector
addressSize_d = 8
min_0_d = 1
min_1_d = 0
step_d = 4
binaryFunc_d = 0
frame_scale_d = 1.0

# gt = []
# file = open("data/images/david_gt.txt", "r")
# lines = file.read().split("\n")
# # print(lines)
# for line in lines:
#     temp = line.split(",")
#     if len(temp) == 4:
#         gt.append([float(temp[0]), float(temp[1]), float(temp[2]), float(temp[3])])
#
# # print(gt)
#
# def euclidean(vector1, vector2):
#     '''calculate the euclidean distance, no numpy
#     input: numpy.arrays or lists
#     return: euclidean distance
#     '''
#     dist = [(a - b)**2 for a, b in zip(vector1, vector2)]
#     dist = math.sqrt(sum(dist))
#     return dist

# training_files = glob.glob("data/CroppedYale/**/*.pgm")
training_files = glob.glob("data/att_faces/**/*.pgm")

print("TRAINING")

# import random
# examples = random.sample(range(len(training_files)), 500)

detector = widetra.Detector(addressSize_d, min_0_d, min_1_d, binaryFunc_d)
for file in training_files:
    img = cv2.resize(cv2.imread(file), (104,127))
    detector.train(img)

# detect_config = old_detector.getJSON()
# file = open("config1.txt", "w")
# file.write(detect_config)
# file.close()
#
# detector = widetra.Detector(detect_config)
#
# detect_config = detector.getJSON()
# file = open("config2.txt", "w")
# file.write(detect_config)
# file.close()

files = glob.glob("data/images/david/*.png")
files.sort()

print("DETECT")
frame = cv2.imread(files[0])

start_time = time.time()
sel = detector.find(frame, step_d, frame_scale_d)
print("--- %s seconds ---" % (time.time() - start_time))
print(sel)
cv2.imshow("teste", cv2.rectangle(frame,(sel[0], sel[1]),(sel[2], sel[3]),(0, 0, 255),1))
if cv2.waitKey(0):
    pass
    # cv2.destroyAllWindows()

fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('girl.avi',fourcc, 24, (np.shape(frame)[1], np.shape(frame)[0]))


# precision = open("precision_david.txt", "w")

print("TRACKING")
tracker = widetra.Tracker(cv2.imread(files[0]), [sel[0], sel[1], sel[2]-sel[0], sel[3]-sel[1]],  max_discriminators, address_size, search_size, new_discriminator, new_training, max_training, min_score, min_0, min_1, initial_scale)
for i, file in enumerate(files):
    frame = cv2.imread(file)
    sel = tracker.find(frame)
    cv2.rectangle(frame, (sel[0], sel[1]), (sel[2], sel[3]), (0, 255, 0));
    cv2.imshow("teste", frame)
    # if i % 5 == 0:
    #     precision.write("{}\n".format(euclidean((sel[0], sel[1]), (gt[i][0], gt[i][1]))))
    out.write(frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# precision.close()
out.release()
cv2.destroyAllWindows()
